<?php

namespace App\Services\Courier;

use App\Criteria\Courier\GetCourierOrdersCriteria;
use App\Criteria\Courier\ShowCourierOrderCriteria;
use App\DTOs\Courier\CreateCourierOrderDto;
use App\DTOs\Courier\GetCourierOrdersDto;
use App\DTOs\Courier\ShowCourierOrderDto;
use App\Enum\CourierOrderEnum;
use App\Enum\OrderEnum;
use App\Exceptions\Code\LogicException;
use App\Repositories\Courier\CourierOrderRepositoryInterface;
use App\Services\Order\CompanyOrderServiceInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class CourierOrderService implements CourierOrderServiceInterface
{
    public function __construct(
        protected CourierOrderRepositoryInterface $courierOrderRepository,
        protected CompanyOrderServiceInterface    $companyOrderService)
    {

    }

    public function getPendingOrders()
    {
        return $this->companyOrderService->getPendingOrders();
    }

    public function acceptOrder(CreateCourierOrderDto $dto)
    {
        try {
            DB::beginTransaction();
            $order = $this->companyOrderService->find($dto->orderId);
            if ($order->status != OrderEnum::STATUS_WAITING) {
                throw new LogicException(Response::HTTP_NOT_FOUND);
            }

            $order->lockForUpdate();
            $this->companyOrderService->updateOrderStatus(OrderEnum::STATUS_IN_ROUTE, $dto->orderId);
            $data = [
                'order_id' => $dto->orderId,
                'user_id'  => $dto->courierId,
                'status'   => CourierOrderEnum::STATUS_ACCEPTED,
            ];
            $order = $this->courierOrderRepository->create($data);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw new LogicException(Response::HTTP_BAD_REQUEST);
        }

        return $order;
    }

    public function index(GetCourierOrdersDto $dto)
    {
        try {
            return $this->courierOrderRepository
                ->with('order.company')
                ->pushcriteria(new GetCourierOrdersCriteria($dto->courierId))
                ->paginate();

        } catch (Exception $e) {
            throw new LogicException(Response::HTTP_NOT_FOUND);
        }
    }

    public function show(ShowCourierOrderDto $dto)
    {
        try {
            return $this->courierOrderRepository
                ->with('order')
                ->pushcriteria(new ShowCourierOrderCriteria($dto->userId, $dto->id))
                ->first();
        } catch (Exception $e) {
            throw new LogicException(Response::HTTP_NOT_FOUND);
        }
    }
}
