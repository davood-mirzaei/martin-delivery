<?php

namespace App\Services\Courier;

use App\DTOs\Courier\CreateCourierOrderDto;
use App\DTOs\Courier\GetCourierOrdersDto;
use App\DTOs\Courier\ShowCourierOrderDto;

interface CourierOrderServiceInterface
{
    public function getPendingOrders();

    public function acceptOrder(CreateCourierOrderDto $dto);

    public function index(GetCourierOrdersDto $dto);

    public function show(ShowCourierOrderDto $dto);
}
