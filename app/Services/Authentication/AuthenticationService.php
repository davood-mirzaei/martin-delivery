<?php

namespace App\Services\Authentication;

use App\Exceptions\LoginFailedException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class AuthenticationService implements AuthenticationServiceInterface
{
    public function login($request)
    {
        $data = [
          'username' => $request->email,
          'password' => $request->password,
        ];

        $loginData = array_merge($data, [
            'grant_type'    => $request->grant_type ?? 'password',
            'client_id'     => Config::get('auth.clients.web.admin.id'),
            'client_secret' => Config::get('auth.clients.web.admin.secret'),
            'scope'         => '',
        ]);

        return $this->callOAuth($loginData);
    }

    public function callOAuth($data)
    {
        $authFullApiUrl = Config::get('app.url') . '/oauth/token';
        $response       = Http::asForm()->post($authFullApiUrl, $data);

        if (!$response->successful()) {
            throw new LoginFailedException(statusCode : $response->status());
        }

        return $response;
    }
}
