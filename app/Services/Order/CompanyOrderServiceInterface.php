<?php

namespace App\Services\Order;

use App\DTOs\Company\CreateCompanyOrderDto;
use App\DTOs\Company\GetCompanyOrdersDto;
use App\DTOs\Company\ShowCompanyOrderDto;
use App\DTOs\Company\UpdateCompanyOrderDto;

interface CompanyOrderServiceInterface
{
    public function create(CreateCompanyOrderDto $dto);

    public function index(GetCompanyOrdersDto $dto);

    public function show(ShowCompanyOrderDto $dto);

    public function find( int $id);

    public function update(UpdateCompanyOrderDto $dto);

    public function updateOrderStatus($status, int $id);

    public function getPendingOrders();

    }
