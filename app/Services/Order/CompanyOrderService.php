<?php

namespace App\Services\Order;

use App\Criteria\Company\GetPendingOrdersCriteria;
use App\Criteria\Company\ShowOrderCriteria;
use App\Criteria\Company\ThisCompanyCriteria;
use App\Criteria\Company\UpdateOrderCriteria;
use App\DTOs\Company\CreateCompanyOrderDto;
use App\DTOs\Company\GetCompanyOrdersDto;
use App\DTOs\Company\ShowCompanyOrderDto;
use App\DTOs\Company\UpdateCompanyOrderDto;
use App\Enum\OrderEnum;
use App\Exceptions\Code\LogicException;
use App\Http\Resources\Company\GetCompanyOrdersResource;
use App\Repositories\Order\OrderRepositoryInterface;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class CompanyOrderService implements CompanyOrderServiceInterface
{
    public function __construct(protected OrderRepositoryInterface $orderRepository)
    {

    }

    public function create(CreateCompanyOrderDto $dto)
    {
        try {
            if (!in_array($dto->companyId, $dto->userCompanyIds)) {
                throw new LogicException(Response::HTTP_FORBIDDEN);
            }

            $data               = $dto->data;
            $data['company_id'] = $dto->companyId;
            $data['status']     = OrderEnum::STATUS_WAITING;

            return $this->orderRepository->create($data);
        } catch (Exception $e) {
            throw new LogicException(Response::HTTP_BAD_REQUEST);
        }

    }

    public function index(GetCompanyOrdersDto $dto)
    {
        try {
            return $this->orderRepository
                ->with('company')
                ->pushcriteria(new ThisCompanyCriteria($dto->companyIds))
                ->orderBy('created_at', 'desc')
                ->paginate();
        } catch (Exception $e){
            throw new LogicException(Response::HTTP_NOT_FOUND);
        }
    }

    public function show(ShowCompanyOrderDto $dto)
    {
        try {
            return $this->orderRepository
                ->with('company')
                ->pushcriteria(new ShowOrderCriteria($dto->orderId, $dto->userCompanyIds))
                ->first();
        }catch (Exception $e){
            throw new LogicException(Response::HTTP_NOT_FOUND);
        }
    }

    public function update(UpdateCompanyOrderDto $dto)
    {
        try {
            $order = $this->orderRepository
                ->pushcriteria(new UpdateOrderCriteria($dto->orderId))
                ->pushcriteria(new ThisCompanyCriteria($dto->userCompanyIds))
                ->first();

            if (!$order){
                throw new LogicException(Response::HTTP_NOT_FOUND);
            }

            $data['status'] = $dto->status;
            $this->orderRepository->update($data, $dto->orderId);
        } catch (Exception $e){
            throw new LogicException(Response::HTTP_NOT_FOUND);
        }
    }

    public function find(int $id)
    {
        return $this->orderRepository->find($id);

    }

    public function updateOrderStatus($status, int $id)
    {
        $data['status'] = $status;
        $this->orderRepository->update($data, $id);

    }

    public function getPendingOrders()
    {
        try {
            return $this->orderRepository
                ->pushcriteria(new GetPendingOrdersCriteria())
                ->paginate();
        }catch (Exception $e){
            throw new LogicException(Response::HTTP_NOT_FOUND);
        }
    }

}
