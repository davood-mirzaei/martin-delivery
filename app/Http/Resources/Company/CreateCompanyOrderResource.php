<?php

namespace App\Http\Resources\Company;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CreateCompanyOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'company_id'            => $this->company_id,
            'parcel_sender_name'    => $this->parcel_sender_name,
            'parcel_sender_mobile'  => $this->parcel_sender_mobile,
            'parcel_sender_address' => $this->parcel_sender_address,
            'receiver_name'         => $this->receiver_name,
            'receiver_mobile'       => $this->receiver_mobile,
            'receiver_address'      => $this->receiver_address,
            'status'                => $this->status,
        ];
    }
}
