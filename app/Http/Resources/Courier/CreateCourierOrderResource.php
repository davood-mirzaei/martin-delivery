<?php

namespace App\Http\Resources\Courier;

use Illuminate\Http\Resources\Json\JsonResource;

class CreateCourierOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'order_id'              => $this->order_id,
            'courier_id'            => $this->user_id,
            'parcel_sender_name'    => $this->order->parcel_sender_name,
            'parcel_sender_mobile'  => $this->order->parcel_sender_mobile,
            'parcel_sender_address' => $this->order->parcel_sender_address,
            'receiver_name'         => $this->order->receiver_name,
            'receiver_mobile'       => $this->order->receiver_mobile,
            'receiver_address'      => $this->order->receiver_address,
            'company_name'          => $this->order->company->name,
        ];
    }
}
