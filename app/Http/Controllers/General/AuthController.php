<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Services\Authentication\AuthenticationService;
use App\Services\Authentication\AuthenticationServiceInterface;

class AuthController extends Controller
{

    public function __construct(protected AuthenticationServiceInterface $service)
    {

    }

    public function loginUsingPasswordGrant(LoginRequest $request)
    {
        $result = $this->service->login($request);

        return $result->json();
    }

}
