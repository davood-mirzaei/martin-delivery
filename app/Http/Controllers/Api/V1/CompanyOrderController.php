<?php

namespace App\Http\Controllers\Api\V1;

use App\DTOs\Company\CreateCompanyOrderDto;
use App\DTOs\Company\GetCompanyOrdersDto;
use App\DTOs\Company\ShowCompanyOrderDto;
use App\DTOs\Company\UpdateCompanyOrderDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CreateCompanyOrderRequest;
use App\Http\Requests\Company\ShowCompanyOrderRequest;
use App\Http\Requests\Company\UpdateCompanyOrderRequest;
use App\Http\Resources\Company\CreateCompanyOrderResource;
use App\Http\Resources\Company\GetCompanyOrdersResource;
use App\Services\Order\CompanyOrderService;
use App\Services\Order\CompanyOrderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CompanyOrderController extends Controller
{
    public function __construct(protected CompanyOrderServiceInterface $service)
    {

    }

    public function create(CreateCompanyOrderRequest $request)
    {
        $dto = CreateCompanyOrderDto::getFromRequest($request);
        $order = $this->service->create($dto);

        return new CreateCompanyOrderResource($order);
    }

    public function index(Request $request): AnonymousResourceCollection
    {
        $dto = GetCompanyOrdersDto::getFromRequest($request);
        $orders = $this->service->index($dto);

        return GetCompanyOrdersResource::collection($orders);
    }

    public function show(ShowCompanyOrderRequest $request)
    {
        $dto = ShowCompanyOrderDto::getFromRequest($request);
        $order = $this->service->show($dto);

        return new GetCompanyOrdersResource($order);
    }

    public function update(UpdateCompanyOrderRequest $request)
    {
        $dto = UpdateCompanyOrderDto::getFromRequest($request);
        $this->service->update($dto);

        return response()->json([
            'message' => 'your order canceled',
            ]);
    }
}
