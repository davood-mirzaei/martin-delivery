<?php

namespace App\Http\Controllers\Api\V1;

use App\DTOs\Courier\CreateCourierOrderDto;
use App\DTOs\Courier\GetCourierOrdersDto;
use App\DTOs\Courier\ShowCourierOrderDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\Courier\CreateCourierOrderRequest;
use App\Http\Requests\Courier\ShowCourierOrderRequest;
use App\Http\Resources\Courier\CreateCourierOrderResource;
use App\Http\Resources\Company\GetCompanyOrdersResource;
use App\Http\Resources\Courier\GetCourierOrdersResources;
use App\Services\Courier\CourierOrderServiceInterface;
use Illuminate\Http\Request;

class CourierOrderController extends Controller
{
    public function __construct(protected CourierOrderServiceInterface $service)
    {
    }

    public function getPendingOrders()
    {
        $orders = $this->service->getPendingOrders();

        return GetCompanyOrdersResource::collection($orders);
    }

    public function acceptOrder(CreateCourierOrderRequest $request)
    {
        $dto = CreateCourierOrderDto::getFromRequest($request);
        $order = $this->service->acceptOrder($dto);

        return new CreateCourierOrderResource($order);
    }

    public function index(Request $request)
    {
        $dto = GetCourierOrdersDto::getFromRrquest($request);
        $orders = $this->service->index($dto);

        return GetCourierOrdersResources::collection($orders);
    }

    public function show(ShowCourierOrderRequest $request)
    {
        $dto = ShowCourierOrderDto::getFromRequest($request);
        $order = $this->service->show($dto);

        return new GetCourierOrdersResources($order);
    }
}
