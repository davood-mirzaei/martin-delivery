<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'company_id' => 'required',
            'parcel_sender_name' => 'required|string',
            'parcel_sender_mobile' => 'required|string',
            'parcel_sender_address' => 'required|string',
            'receiver_name' => 'required|string',
            'receiver_mobile' => 'required|string',
            'receiver_address' => 'required|string',
        ];
    }
}
