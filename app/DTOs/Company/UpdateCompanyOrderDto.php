<?php

namespace App\DTOs\Company;

use Illuminate\Http\Request;
use function PHPUnit\Framework\isNull;

class UpdateCompanyOrderDto
{
    public function __construct(
        public int    $orderId,
        public string $status,
        public array  $userCompanyIds
    )
    {

    }

    public static function getFromRequest(Request $request)
    {
        $companyIds = [];
        if (!is_Null($request->user()->companies)){
            $companyIds = array_map(function ($item){
                return $item['id'];
            }, $request->user()->companies->toArray());

            return new static($request->id, $request->status, $companyIds);
        }
    }
}
