<?php

namespace App\DTOs\Company;

use Illuminate\Http\Request;

class CreateCompanyOrderDto
{
    public function __construct(
       public int $companyId,
       public array $data,
       public array $userCompanyIds
    )
    {
    }

    public static function getFromRequest(Request $request): CreateCompanyOrderDto
    {
        $companyIds = [];
        if (!is_null($request->user()->companies)){
            $companyIds = array_map(function ($item){
                return $item['id'];
            }, $request->user()->companies->toArray());
        }

        return new static($request->company_id, $request->validated(), $companyIds);
    }
}
