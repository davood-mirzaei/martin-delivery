<?php

namespace App\DTOs\Company;

use Illuminate\Http\Request;

class ShowCompanyOrderDto
{
    public function __construct(
        public array $userCompanyIds,
        public int   $orderId
    )
    {

    }

    public static function getFromRequest(Request $request)
    {
        $companyIds = [];
        if (!is_null($request->user()->companies)){
            $companyIds = array_map(function ($item){
                return $item['id'];
            }, $request->user()->companies->toArray());
        }

        return new static($companyIds, $request->id);
    }
}
