<?php

namespace App\DTOs\Company;

use Illuminate\Http\Request;

class GetCompanyOrdersDto
{
    public function __construct(public array $companyIds)
    {

    }

    public static function getFromRequest(Request $request): GetCompanyOrdersDto
    {
        $companyIds = [];
        if (!is_null($request->user()->companies)){
            $companyIds = array_map(function ($item){
               return $item['id'];
            }, $request->user()->companies->toArray());
        }

        return new static($companyIds);
    }

}
