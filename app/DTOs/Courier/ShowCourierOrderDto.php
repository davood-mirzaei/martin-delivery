<?php

namespace App\DTOs\Courier;

use Illuminate\Http\Request;

class ShowCourierOrderDto
{
    public function __construct(
        public int $userId,
        public int $id,
    )
    {

    }

    public static function getFromRequest(Request $request)
    {
        return new static($request->user()->id,$request->id);
    }
}
