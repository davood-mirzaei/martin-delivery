<?php

namespace App\DTOs\Courier;


use Illuminate\Http\Request;

class GetCourierOrdersDto
{
    public function __construct(public int $courierId)
    {

    }

    public static function getFromRrquest(Request $request)
    {
        return new static($request->user()->id);
    }
}
