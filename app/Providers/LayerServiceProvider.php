<?php

namespace App\Providers;

use App\Services\Authentication\AuthenticationService;
use App\Services\Authentication\AuthenticationServiceInterface;
use App\Services\Courier\CourierOrderService;
use App\Services\Courier\CourierOrderServiceInterface;
use App\Services\Order\CompanyOrderService;
use App\Services\Order\CompanyOrderServiceInterface;
use Illuminate\Support\ServiceProvider;

class LayerServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(AuthenticationServiceInterface::class, AuthenticationService::class);
        $this->app->bind(CompanyOrderServiceInterface::class, CompanyOrderService::class);
        $this->app->bind(CourierOrderServiceInterface::class, CourierOrderService::class);
    }

    public function boot()
    {
        //
    }
}
