<?php

namespace App\Providers;

use App\Repositories\Courier\CourierOrderRepository;
use App\Repositories\Courier\CourierOrderRepositoryInterface;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Order\OrderRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
        $this->app->bind(CourierOrderRepositoryInterface::class, CourierOrderRepository::class);
    }
}
