<?php

namespace App\Repositories\Courier;

use App\Models\CourierOrder;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Courier\CourierOrderRepositoryInterface;
//use App\Entities\Courier\CourierOrder;
//use App\Validators\Courier\CourierOrderValidator;

/**
 * Class CourierOrderRepository.
 *
 * @package namespace App\Repositories\Courier;
 */
class CourierOrderRepository extends BaseRepository implements CourierOrderRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CourierOrder::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
