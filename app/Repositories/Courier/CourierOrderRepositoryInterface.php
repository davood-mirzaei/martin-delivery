<?php

namespace App\Repositories\Courier;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CourierOrderRepositoryInterface.
 *
 * @package namespace App\Repositories\Courier;
 */
interface CourierOrderRepositoryInterface extends RepositoryInterface
{
    //
}
