<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'parcel_sender_name',
        'parcel_sender_mobile',
        'parcel_sender_address',
        'receiver_name',
        'receiver_mobile',
        'receiver_address',
        'status',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
