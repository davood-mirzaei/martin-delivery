<?php

namespace App\Criteria\Company;

use App\Enum\OrderEnum;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class GetPendingOrdersCriteria.
 *
 * @package namespace App\Criteria\Company;
 */
class GetPendingOrdersCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('status', '=', OrderEnum::STATUS_WAITING);
    }
}
