<?php

namespace App\Criteria\Company;

use App\Enum\OrderEnum;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UpdateOrderCriteria.
 *
 * @package namespace App\Criteria\Company;
 */
class UpdateOrderCriteria implements CriteriaInterface
{
    public function __construct(
        private int $orderId,
    )
    {

    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('id' ,$this->orderId)->where('status', OrderEnum::STATUS_WAITING);
    }
}
