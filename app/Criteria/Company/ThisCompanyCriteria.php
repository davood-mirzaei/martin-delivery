<?php

namespace App\Criteria\Company;

use App\Repositories\Order\OrderRepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ThisCompanyCriteria.
 *
 * @package namespace App\Criteria\Company;
 */
class ThisCompanyCriteria implements CriteriaInterface
{
    public function __construct(private array $userCompanyIds)
    {

    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository): mixed
    {
        return $model->whereIn('company_id', $this->userCompanyIds);
    }
}
