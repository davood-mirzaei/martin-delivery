<?php

namespace App\Criteria\Company;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ShowOrderCriteria.
 *
 * @package namespace App\Criteria\Company;
 */
class ShowOrderCriteria implements CriteriaInterface
{
    public function __construct(
        private int $orderId,
        private array $companyIds,
    )
    {

    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('id', $this->orderId)
            ->whereIn('company_id', $this->companyIds);
    }
}
