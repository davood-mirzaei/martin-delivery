<?php

namespace App\Criteria\Courier;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class GetCourierOrdersCriteria.
 *
 * @package namespace App\Criteria\Courier;
 */
class GetCourierOrdersCriteria implements CriteriaInterface
{
    public function __construct(private int $courierId)
    {

    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('user_id', $this->courierId);
    }
}
