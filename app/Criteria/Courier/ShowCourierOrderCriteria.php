<?php

namespace App\Criteria\Courier;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ShowCourierOrderCriteria.
 *
 * @package namespace App\Criteria\Courier;
 */
class ShowCourierOrderCriteria implements CriteriaInterface
{
    public function __construct(
        private int $userId,
        private int $id,
    )
    {

    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('id', $this->id)->where('user_id', $this->userId);
    }
}
