<?php

namespace App\Exceptions\Code;

class CustomErrorCodesTable
{
    const LOGIN_FAILED = [
        'code'        => 3000,
        'title'       => 'اطلاعات ورود صحیح نمیباشد.',
        'description' => 'An Exception Happened During The Login Process.',
    ];

    const UPDATE_USER_FAILED = [
        'code'        => 3001,
        'title'       => 'بروزرسانی کاربر با خطا مواجه شد.',
        'description' => 'Update User Failed.',
    ];

    const UPDATE_PASSWORD_FAILED = [
        'code'        => 3002,
        'title'       => 'بروزرسانی کلمه عبور با خطا مواجه شد.',
        'description' => 'Update Password Failed.',
    ];

    const UPDATE_COURSE_FAILED = [
        'code'        => 3003,
        'title'       => 'بروزرسانی درس با خطا مواجه شد.',
        'description' => 'Update Course Failed.',
    ];

    const UPDATE_PRESENTCOURSE_FAILED = [
        'code'        => 3004,
        'title'       => 'بروزرسانی درس ارايه شده با خطا مواجه شد.',
        'description' => 'Update PresentCourse Failed.',
    ];

    const GET_PROFESSOR_BYID_FAILED = [
        'code'        => 3005,
        'title'       => 'دریافت مشخصات  مدرس با خطا مواجه شد.',
        'description' => 'Get Professor ById Failed.',
    ];

    const ADD_STUDENTCOURSE_FAILED = [
        'code'        => 3006,
        'title'       => 'انتخاب واحد با خطا مواجه شد.',
        'description' => 'Add StudentCourse Failed.',
    ];
}
