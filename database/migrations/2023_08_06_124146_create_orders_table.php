<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')->constrained('companies')
            ->cascadeOnUpdate()->cascadeOnDelete();
            $table->string('parcel_sender_name');
            $table->string('parcel_sender_mobile');
            $table->text('parcel_sender_address');
            $table->string('receiver_name');
            $table->string('receiver_mobile');
            $table->string('receiver_address');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
