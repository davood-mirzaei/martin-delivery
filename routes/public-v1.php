<?php


use App\Http\Controllers\General\AuthController;
use Illuminate\Support\Facades\Route;

Route::post('login', [AuthController::class, 'loginUsingPasswordGrant'])->name('login');

