<?php


use App\Http\Controllers\Api\V1\CompanyOrderController;
use App\Http\Controllers\Api\V1\CourierOrderController;
use Illuminate\Support\Facades\Route;

Route::prefix('company')->group(function (){
    Route::prefix('orders')->group(function (){
        Route::post('', [CompanyOrderController::class, 'create']);
        Route::get('', [CompanyOrderController::class, 'index']);
        Route::get('/{id}', [CompanyOrderController::class, 'show']);
        Route::patch('/{id}', [CompanyOrderController::class, 'update']);
    });
});

Route::prefix('courier')->group(function (){
    Route::prefix('orders')->group(function (){
        Route::post('', [CourierOrderController::class, 'acceptOrder']);
        Route::get('/pending', [CourierOrderController::class, 'getPendingOrders']);
        Route::get('', [CourierOrderController::class, 'index']);
        Route::get('/{id}', [CourierOrderController::class, 'show']);
    });
});
